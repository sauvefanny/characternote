
CREATE DATABASE character_note
    DEFAULT CHARACTER SET = 'utf8mb4';


    DROP TABLE IF EXISTS object;
    DROP TABLE IF EXISTS skills;

    DROP TABLE IF EXISTS game;

    DROP TABLE IF EXISTS characters;

    CREATE TABLE figure (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    char_name VARCHAR(50) NOT NULL,
    background VARCHAR(512)
    );

    CREATE TABLE equipment (
        id INTEGER AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(50) NOT NULL,
        effect VARCHAR(255) NOT NULL,
        id_character INTEGER,

        CONSTRAINT fk_equip_character FOREIGN KEY (id_character)
        REFERENCES figure(id)
    );


    CREATE TABLE skill(
        id INTEGER AUTO_INCREMENT,
        name VARCHAR(50) NOT NULL,
        mana_cost int,
        genre VARCHAR(50),
        CONSTRAINT fk_skills_figure FOREIGN KEY (id)
        REFERENCES figure(id)
    );

    CREATE TABLE game(
        id INTEGER AUTO_INCREMENT,
        name VARCHAR(50) NOT NULL,
        genre VARCHAR(50),
        CONSTRAINT fk_game_character FOREIGN KEY (id)
        REFERENCES characters(id)

    );

    CREATE TABLE charToSkill(
    skill_id INTEGER NOT NULL,
    chara_id INTEGER NOT NULL PRIMARY KEY,
    CONSTRAINT fk_char_comp FOREIGN KEY (comp_id)
    REFERENCES skill(id),
    CONSTRAINT fk_comp_chara FOREIGN KEY (chara_id)
    REFERENCES characters(id)
);

CREATE TABLE chartogame(
    game_id INTEGER NOT NULL,
    chara_id INTEGER NOT NULL PRIMARY KEY,
    CONSTRAINT fk_char_game FOREIGN KEY (game_id)
    REFERENCES game(id),
    CONSTRAINT fk_game_chara FOREIGN KEY (chara_id)
    REFERENCES character(id)
);



