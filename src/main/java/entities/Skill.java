package entities;

public class Skill {
    private int id;
    private String name;
    private int mana_cost;
    private String genre;

    public Skill(int id, String name, int mana_cost, String genre) {
        this.id = id;
        this.name = name;
        this.mana_cost = mana_cost;
        this.genre = genre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMana_cost() {
        return mana_cost;
    }

    public void setMana_cost(int mana_cost) {
        this.mana_cost = mana_cost;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }
}
