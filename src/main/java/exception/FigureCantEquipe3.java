package exception;

public class FigureCantEquipe3 extends Exception {
    public FigureCantEquipe3(){
        super("Can't have more than 3 pieces of armory.");
    }
}
