package co.simplon.cda.chracternote.repo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Repository;

import co.simplon.cda.chracternote.entities.Equipement;


@Repository
public class EquipementRepository {
    @Autowired
    private DataSource dataSource;

    public List<Equipement> findAll() {
        List<Equipement>  list = new ArrayList<>();

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM equipement");
            
            ResultSet result = stmt.executeQuery();
            while(result.next()) {
                Equipement equipement = new Equipement(
                    result.getInt("id"),
                    result.getString("name"), 
                    result.getString("effect"), 
                    result.getBoolean("isSpecial"));
                list.add(equipement);
            }
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return list;
    }

    public Equipement findById(int id) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM equipement WHERE id=?");
            stmt.setInt(1, id);
            
            ResultSet result = stmt.executeQuery();
            if(result.next()) {
                Equipement equipement = new Equipement(
                    result.getInt("id"),
                    result.getString("name"), 
                    result.getString("effect"), 
                    result.getBoolean("isSpecial"));
                return equipement;
            }
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }

        return null;
    }


    public boolean delete(int id) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM equipement WHERE id=?");
            stmt.setInt(1, id);
            
            return stmt.executeUpdate() == 1;
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }
    
    public boolean update(Equipement equipement) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("UPDATE equipement SET name=?, effect=?, isSpecial=? WHERE id=?");

            stmt.setString(1, equipement.getName());
            stmt.setString(2, equipement.getEffect());
            stmt.setBoolean(3, equipement.getIsSpecial());
            stmt.setInt(4, equipement.getId());
            
            return stmt.executeUpdate() == 1;
            
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }

    public boolean add(Equipement equipement) {

        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO equipement (name,effect,isSpecial) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, equipement.getName());
            stmt.setString(2, equipement.getEffect());
            stmt.setBoolean(3, equipement.getIsSpecial());
            
            if(stmt.executeUpdate() == 1) {
                //Ici, on choppe l'id auto incrémenté et on l'assigne à notre equipement
                ResultSet rsKeys = stmt.getGeneratedKeys();
                rsKeys.next();
                equipement.setId(rsKeys.getInt(1));
                return true;
            }
            
            
        } catch (SQLException e) {
            
            e.printStackTrace();
        } finally {
            DataSourceUtils.releaseConnection(connection, dataSource);
        }
        return false;
    }
}

