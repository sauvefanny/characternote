package co.simplon.cda.chracternote.entities;

public class Figure {
    private int id;
    private String name;
    private String bg;
    
    
    
    public Figure(int id, String name, String bg){
        this.id = id;
        this.name = name;
        this.bg = bg;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }
    

}

