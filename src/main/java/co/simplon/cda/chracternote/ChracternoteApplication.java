package co.simplon.cda.chracternote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import co.simplon.cda.chracternote.entities.Window;

@SpringBootApplication
public class ChracternoteApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChracternoteApplication.class, args);

		new Window();
	}

}