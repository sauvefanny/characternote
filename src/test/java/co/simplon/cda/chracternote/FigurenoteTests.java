package co.simplon.cda.chracternote;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.ArrayList;
import java.util.List;

import com.jayway.jsonpath.internal.function.numeric.Min;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import entities.Equipement;

@SpringBootTest
class FigureTests {

    @Test
    void getListEquipementWithNotNullValue() {
        
        List<Equipement> list = new ArrayList<Equipement>();
        assertEquals(true, list.size());


    }

}

// Personnage doit avoir une liste d'équipement.
// (List<equipement>) Le test doit vérifier que, lorsque l'on crée un nouveau
// personnage, cette liste ne soit pas vide. pour cela, utiliser List.getsize,
// par exemple. Dans le cas ou la liste est vide, le test renvoie une erreur.
// Exemple : Je veux créer le personnage "Link". Pour que cela fonctionne, je ne
// peux pas juste lui donnée son nom et son age. Il faut que la liste
// d'équipement ait au moins une entrée (par exemple, la master sword)